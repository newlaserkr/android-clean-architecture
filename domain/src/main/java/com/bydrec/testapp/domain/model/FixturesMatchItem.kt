package com.bydrec.testapp.domain.model

import org.threeten.bp.DayOfWeek

data class FixturesMatchItem(
    var competitionName: String? = null,
    var venueName: String? = null,
    var dateAndTime: String? = null,
    var homeTeam: String? = null,
    var awayTeam: String? = null,
    var dayofMonth: Int? = null,
    var dayOfWeek: DayOfWeek? = null,
    var postponed: Boolean? = null,
    var isHeader: Boolean = false,
    var headerDate: String? = null,
    var time: Long? = null
){

}