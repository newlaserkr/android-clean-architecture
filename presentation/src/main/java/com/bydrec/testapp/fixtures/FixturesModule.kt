package com.bydrec.testapp.fixtures

import com.bydrec.testapp.data.repositories.FixturesRepositoryImpl
import com.bydrec.testapp.data.restservices.FixturesRestService
import com.bydrec.testapp.data.restservices.impl.FixturesRetrofitServiceImpl
import com.bydrec.testapp.domain.repositories.FixturesRepository
import com.bydrec.testapp.domain.usecases.FixturesUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val fixturesModule : Module = module {
    viewModel { FixturesViewModel(get()) }
    single { FixturesUseCase(get()) }
    single<FixturesRepository> { FixturesRepositoryImpl(get()) }
    single<FixturesRestService> { FixturesRetrofitServiceImpl() }

}

