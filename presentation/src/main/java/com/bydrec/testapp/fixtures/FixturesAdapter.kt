package com.bydrec.testapp.fixtures

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bydrec.testapp.R
import com.bydrec.testapp.domain.model.FixturesMatchItem
import kotlinx.android.synthetic.main.fixtures_item.view.*
import kotlinx.android.synthetic.main.header_item.view.*
import org.threeten.bp.format.TextStyle
import java.util.*


class FixturesAdapter(private val items: List<FixturesMatchItem>) : RecyclerView.Adapter<FixturesAdapter.ViewHolder>() {

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        open fun bind(item: FixturesMatchItem){}
    }

    class ViewHolderHeader(view: View) : ViewHolder(view) {

        override fun bind(item: FixturesMatchItem) {
            with(itemView){
                txtHeader.text = item.headerDate
            }
        }

    }

    class ViewHolderItem(view: View) : ViewHolder(view) {

        override fun bind(item: FixturesMatchItem) {
            with(itemView){
                txtAwayTeam.text = item.awayTeam
                txtCompetitionName.text = item.competitionName?.toUpperCase()
                txtDateAndTime.text = item.dateAndTime
                txtDayOfWeek.text = item.dayOfWeek?.getDisplayName(TextStyle.SHORT, Locale.UK)?.toUpperCase()
                txtDayofMonth.text = item.dayofMonth.toString()
                txtHomeTeam.text = item.homeTeam
                txtVenueName.text = item.venueName

                if(item.postponed == true){
                    txtPostponed.visibility = View.VISIBLE
                    txtDateAndTime.setTextColor(ContextCompat.getColor(context, R.color.colorOrange))
                } else {
                    txtPostponed.visibility = View.GONE
                    txtDateAndTime.setTextColor(ContextCompat.getColor(context, android.R.color.black))
                }
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == HEADER) {
            ViewHolderHeader(LayoutInflater.from(parent.context).inflate(R.layout.header_item, parent, false))
        } else{
            ViewHolderItem(LayoutInflater.from(parent.context).inflate(R.layout.fixtures_item, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(items[position].isHeader) HEADER else ITEM
    }

    companion object{
        const val HEADER = 0
        const val ITEM = 1
    }

}