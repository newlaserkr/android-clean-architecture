package com.bydrec.testapp.fixtures

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.bydrec.testapp.domain.model.FixturesMatchItem
import com.bydrec.testapp.domain.usecases.FixturesUseCase
import kotlinx.coroutines.*

class FixturesViewModel(private val fixturesUseCase: FixturesUseCase) : ViewModel() {
    private val job = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    //items

    private val items: MutableLiveData<List<FixturesMatchItem>> by lazy {
        MutableLiveData<List<FixturesMatchItem>>().also {
            loadItems(FixturesUseCase.ALL_COMPETITIONS)
        }
    }

    fun getItems(): LiveData<List<FixturesMatchItem>> {
        return items
    }

    fun loadItems(competition: String) {
        uiScope.launch {
            items.value = fixturesUseCase.getItems(competition)
        }
    }

    //competitions

    private val competitions: MutableLiveData<List<String>> by lazy {
        MutableLiveData<List<String>>().also {
            loadCompetitions()
        }
    }

    fun getCompetitions(): LiveData<List<String>> {
        return competitions
    }

    private fun loadCompetitions() {
        uiScope.launch {
            competitions.value = fixturesUseCase.getCompetitions()
        }
    }

}