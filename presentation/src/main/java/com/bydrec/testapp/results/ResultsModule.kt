package com.bydrec.testapp.results

import com.bydrec.testapp.data.repositories.ResultsRepositoryImpl
import com.bydrec.testapp.data.restservices.ResultsRestService
import com.bydrec.testapp.data.restservices.impl.ResultsRetrofitServiceImpl
import com.bydrec.testapp.domain.repositories.ResultsRepository
import com.bydrec.testapp.domain.usecases.ResultsUseCase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

val resultsModule : Module = module {
    viewModel { ResultsViewModel(get()) }
    single { ResultsUseCase(get()) }
    single<ResultsRepository> { ResultsRepositoryImpl(get()) }
    single<ResultsRestService> { ResultsRetrofitServiceImpl() }
}

