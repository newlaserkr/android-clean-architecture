package com.bydrec.testapp.data.restservices.impl

import com.bydrec.testapp.data.entities.results.ResultsResponse
import com.bydrec.testapp.data.restservices.ResultsRestService
import com.bydrec.testapp.data.restservices.RetrofitService
import kotlinx.coroutines.Deferred

class ResultsRetrofitServiceImpl : ResultsRestService, RetrofitService<ResultsRestService>(ResultsRestService::class.java) {

    override fun results(): Deferred<List<ResultsResponse>> {
        return service.results()
    }

}