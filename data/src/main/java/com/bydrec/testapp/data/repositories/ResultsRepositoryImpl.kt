package com.bydrec.testapp.data.repositories

import com.bydrec.testapp.data.entities.results.ResultsResponse
import com.bydrec.testapp.data.restservices.ResultsRestService
import com.bydrec.testapp.domain.model.ResultsMatchItem
import com.bydrec.testapp.domain.repositories.ResultsRepository

class ResultsRepositoryImpl(val resultsRestService: ResultsRestService) : ResultsRepository{

    suspend override fun getItems(): List<ResultsMatchItem> {
        val itemList = mutableListOf<ResultsMatchItem>()
        val resultResponse = resultsRestService.results().await()
        resultResponse.forEach {
            itemList.add(ResultsResponse.transform(it))
        }
        return itemList
    }

}