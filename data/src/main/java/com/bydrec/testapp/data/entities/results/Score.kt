package com.bydrec.testapp.data.entities.results

data class Score(
	val away: Int? = null,
	val winner: String? = null,
	val home: Int? = null
)
